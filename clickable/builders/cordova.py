import json
import shutil
import os

from clickable.config.constants import Constants
from clickable.logger import logger

from .cmake import CMakeBuilder


class CordovaBuilder(CMakeBuilder):
    name = Constants.CORDOVA

    # Lots of this code was based off of this:
    # https://github.com/apache/cordova-ubuntu/blob/28cd3c1b53c1558baed4c66cb2deba597f05b3c6/bin/templates/project/cordova/lib/build.js#L59-L131
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.platform_dir = os.path.join(self.config.cwd, 'platforms/ubuntu/')

        self.config.src_dir = os.path.join(self.platform_dir, 'build')

        if not os.path.isdir(self.platform_dir):
            self.container.run_command("cordova platform add ubuntu")

    def make_install(self):
        super().make_install()
        copies = {
            'www': None,
            'platform_www': 'www',
            'config.xml': None,
            'cordova.desktop': None,
            'manifest.json': None,
            'apparmor.json': None,
        }

        # If value is none, set to key
        copies = {key: key if value is None else value
                  for key, value in copies.items()}

        # Is this overengineerd?
        for file_to_copy_source, file_to_copy_dest in copies.items():
            full_source_path = os.path.join(self.platform_dir,
                                            file_to_copy_source)
            full_dest_path = os.path.join(self.config.install_dir,
                                          file_to_copy_dest)
            if os.path.isdir(full_source_path):
                shutil.copytree(full_source_path, full_dest_path, dirs_exist_ok=True)
            else:
                shutil.copy(full_source_path, full_dest_path)

        apparmor_file = os.path.join(self.config.install_dir, 'apparmor.json')
        with open(apparmor_file, 'r', encoding='UTF-8') as apparmor_reader:
            apparmor = json.load(apparmor_reader)
            apparmor['policy_version'] = 16.04

            if 'webview' not in apparmor['policy_groups']:
                apparmor['policy_groups'].append('webview')

            with open(apparmor_file, 'w', encoding='UTF-8') as apparmor_writer:
                json.dump(apparmor, apparmor_writer, indent=4)

        logger.warning("The cordova builder is deprecated '\
                '(https://gitlab.com/clickable/clickable/-/issues/407).")
